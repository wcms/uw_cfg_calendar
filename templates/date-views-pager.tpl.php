<?php

/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 * - $plugin: The pager plugin object. This contains the view as well as a lot.
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   Urls for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 *
 * $block:
 *   Whether or not this view is in a block.
 *
 * $view
 *   The view object for this navigation.
 *
 * $pager_prefix
 *   Additional information that might be added by overriding
 *   template_process_date_views_pager().
 *
 * $extra_classes
 *   Extra classes for the wrapper, if any.
 */
// Ensure there is always a visible header.
if ((string) $nav_title === '') {
  $nav_title = 'Current month';
}
?>
<?php print !empty($pager_prefix) ? $pager_prefix : ''; ?>
<div class="date-nav-wrapper clearfix<?php print !empty($extra_classes) ? $extra_classes : ''; ?>">
  <div class="date-nav">
    <div class="date-prev">
      <?php if (!empty($prev_url)) : ?>
        <?php
        $pos = strpos($prev_url, '?mini=');
        $pos2 = strpos($prev_url, '&mini=');
        if ($pos !== FALSE && !$mini) {
          $prev_url_array = explode("?", $prev_url);
          $prev_url = $prev_url_array[0];
        }
        if ($pos2 !== FALSE && !$mini) {
          $prev_date = explode("?", $prev_url);
          $prev_date_array = explode("/", $prev_date[0]);
          $prev_url_array = explode("mini=", $prev_url);
          $prev_url = $prev_url_array[0] . 'mini=' . end($prev_date_array);
        }
        ?>
        <?php print l('&laquo;' . ($mini ? '' : t('Previous', array(), array('context' => 'date_nav'))), $prev_url, $prev_options); ?>
      <?php endif; ?>
    </div>
    <div class="date-heading">
      <?php if ($mini) {?>
        <h3><?php print $nav_title; ?></h3>
      <?php } else {?>
        <h2><?php print $nav_title; ?></h2>
      <?php }?>
      <?php
        if(isset($uw_calendar_navigate) && !$mini) {
          print render($uw_calendar_navigate);
        }
      ?>
    </div>
    <div class="date-next">
      <?php if (!empty($next_url)) : ?>
        <?php
        $pos = strpos($next_url, '?mini=');
        $pos2 = strpos($prev_url, '&mini=');
        if ($pos !== FALSE && !$mini) {
          $next_url_array = explode("?", $next_url);
          $next_url = $next_url_array[0];
        }
        if ($pos2 !== FALSE && !$mini) {
          $next_date = explode("?", $next_url);
          $next_date_array = explode("/", $next_date[0]);
          $next_url_array = explode("mini=", $next_url);
          $next_url = $next_url_array[0] . 'mini=' . end($next_date_array);
        }
        ?>
        <?php print l(($mini ? '' : t('Next', array(), array('context' => 'date_nav'))) . ' &raquo;', $next_url, $next_options); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
