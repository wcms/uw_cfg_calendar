/**
 * @file
 */

(function ($) {
  Drupal.behaviors.uw_cfg_calendar = {
    attach: function (context, settings) {

      // Function to step through each single and multi day
      // row class and see if we need show/hide.
      function uw_calendar_add_show_hide(selector) {

        // Step through each column class and see if we need show/hide.
        $(selector).find('td.single-day').each(function () {

          // If we find more than one date (if), add show/hide and
          // hide the entries for that day.
          // If there is only one entry (else), show that entry.
          if ($(this).find('.inner .item').length > 2 && $(this).find('.uw-calendar-show').length == 0) {
            $(this).find('.inner').before('<div class="uw-calendar-show">Show all</div>');
            $(this).find('.inner').before('<div class="uw-calendar-hide">Hide all</div>');
            $(this).find('.views-field-title').css('display', 'none');
            $(this).find('.views-field-title').css('height', '0');
          }
          else if ($(this).find('.inner .item').length <= 2) {
            $(this).find('.views-field-title').css('display', 'block');
            $(this).find('.views-field-title').css('height', 'auto');
          }
        });

        // If the show button is clicked, show dates and hide button.
        $('.uw-calendar .uw-calendar-show').click(function () {
          $(this).parent().find('.uw-calendar-hide').css('display', 'block');
          $(this).parent().find('.uw-calendar-show').css('display', 'none');

          $(this).parent().find('.inner .item').each(function () {
            $(this).find('.views-field-title').css('display', 'block');
            $(this).find('.views-field-title').css('height', 'auto');
          });

        });

        // If the hide button is clicked, remove the dates and add show button.
        $('.uw-calendar .uw-calendar-hide').click(function () {
          $(this).parent().find('.uw-calendar-hide').css('display', 'none');
          $(this).parent().find('.uw-calendar-show').css('display', 'block');

          $(this).parent().find('.inner .item').each(function () {
            $(this).find('.views-field-title').css('display', 'none');
            $(this).find('.views-field-title').css('height', '0');
          });
        });
      }

      // Step through each single day row class and see if we need show/hide.
      $('.uw-calendar tr.single-day').each(function () {
        uw_calendar_add_show_hide($(this));
      });

      // Step through each multi day row class and see if we need show/hide.
      $('.uw-calendar tr.multi-day').each(function () {
        uw_calendar_add_show_hide($(this));
      });
    }
  };
})(jQuery);
